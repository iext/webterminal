FROM python:3.6-jessie
WORKDIR /opt/webterminal
RUN apt-get update && apt-get install -y libmysqlclient-dev redis-server git supervisor nginx \
&& sed -i 's/bind 127.0.0.1 ::1/bind 127.0.0.1/g' /etc/redis/redis.conf \
&& pip install -U pip && git clone https://bitbucket.org/iext/webterminal.git . \
&& mkdir -p ./media/admin/Download && pip install -r requirements.txt
ENV DATABASE_ENGINE mysql
ENV DATABASE_NAME webterminal
ENV DATABASE_USERNAME root
ENV DATABASE_PASSWORD root
ENV DATABASE_HOST 127.0.0.1
ENV DATABASE_PORT 3306
ENV SESSION_COOKIE_AGE 1800
ENV SECRET_KEY $@naaul9f4zi*3s%bze)5cq)q5ufwi!gj5do=area84pimi9p*
ADD nginx.conf /etc/nginx/nginx.conf
ADD supervisord.conf /etc/supervisor/supervisord.conf
ADD docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
EXPOSE 80
CMD ["/docker-entrypoint.sh", "start"]